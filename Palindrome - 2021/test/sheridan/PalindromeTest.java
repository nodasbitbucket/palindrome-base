package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		String word = "ana";
		assertTrue("Cannot validate palindrome word", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		String word = "sheridan";
		assertFalse("Cannot validate palindrome word", Palindrome.isPalindrome(word));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		String word = "top pot";
		assertTrue("Cannot validate palindrome word", Palindrome.isPalindrome(word));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		String word = "top and pot";
		assertFalse("Cannot validate palindrome word", Palindrome.isPalindrome(word));
	}	
	
}
